from . import models
import factory
import random
import datetime


class ClientFactory(factory.Factory):
    class Meta:
        model = models.Customer

    iin = factory.LazyAttribute(lambda n: str(random.randrange(100000000000, 999999999999)))
    phone_number = factory.LazyAttribute(lambda n: str(random.randrange(1000000000, 9999999999)))
    first_name = factory.Faker('first_name')
    last_name = factory.Faker('last_name')


class CreditFactory(factory.Factory):
    class Meta:
        model = models.Credit

    date_created = factory.LazyFunction(datetime.datetime.now)
    amount = factory.LazyAttribute(lambda n: random.randrange(models.Credit.MIN_AMOUNT, models.Credit.MAX_AMOUNT, models.Credit.MIN_AMOUNT))
    customer = factory.SubFactory(ClientFactory)

    @classmethod
    def active_credit(cls):
        credit = CreditFactory()
        credit.to_status_active()
        return credit

    @classmethod
    def paid_credit(cls):
        credit = CreditFactory.active_credit()
        credit.to_status_paid()
        return credit
